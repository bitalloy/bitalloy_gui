// VERSION: 17

importScripts('https://cdnjs.cloudflare.com/ajax/libs/cache.adderall/1.0.0/cache.adderall.js');

var STATIC_FILES = [
        
        'https://bitalloy.gitlab.io/bitalloy_gui/img/512x512',
        'https://bitalloy.gitlab.io/bitalloy_gui/img/192x192',
        'https://bitalloy.gitlab.io/bitalloy_gui/js/faceless.js',
        'https://unpkg.com/onsenui/css/onsenui.css',
        'https://bitalloy.gitlab.io/bitalloy_gui/css/onsen-css-components.css',
        'https://unpkg.com/onsenui/css/ionicons/css/ionicons.css',
        'https://unpkg.com/onsenui/js/onsenui.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.js',
        'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js',
        'https://unpkg.com/onsenui/js/onsenui.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/cash/3.0.0-beta.1/cash.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/cache.adderall/1.0.0/cache.adderall.js',
        'https://bitalloy.gitlab.io/bitalloy_gui/js/bitAlloy.js'
];

var MUTABLE_FILES = [
  'https://bitalloy.gitlab.io/bitalloy_gui/manifest.json',
  'https://bitalloy.gitlab.io/bitalloy_gui/index.html'
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open('cache-v2').then(cache =>
      adderall.addAll(cache, STATIC_FILES, MUTABLE_FILES)
    )
  );
});


// 2. Intercept requests and return the cached version instead
self.addEventListener('fetch', function(e) {
  e.respondWith(
    // check if this file exists in the cache
    caches.match(e.request)
      // Return the cached file, or else try to get it from the server
      .then(response => response || fetch(e.request))
  );
});


